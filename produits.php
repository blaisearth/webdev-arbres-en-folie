<?php
session_start();

include "php/varSession.inc.php";

$category = $_GET['category'];

$cnx = db_connect();

$categ_id = get_category_id($cnx, $category);

if ($categ_id === NULL) {
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	header("Location: http://$host$uri/");
	db_close($cnx);
	exit();
}

$data = get_products($cnx, $categ_id);

db_close($cnx);
?>

<!DOCTYPE html>
<html <?php echo class_theme(); ?>>

<head>
	<title>Arbres en folie</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/master.css" />
	<link rel="stylesheet" type="text/css" href="css/product.css" />
	<script type="text/javascript" src="js/produit.js"></script>
	<script src="js/main.js"></script>
</head>

<body>

	<?php include 'php/header.inc.php'; ?>

	<div id="main">
		<?php include 'php/menu.inc.php'; ?>
		<main>
			<button id="toggler">Afficher le stock</button>
			<table>
				<tbody>
					<tr id="tableheader">
						<td>Aperçu du produit</td>
						<td>Référence</td>
						<td>Description</td>
						<td>Prix</td>
						<td>Commander</td>
						<td class="td-stock" hidden>Stock</td>
					</tr>

					<?php
					foreach ($data as $value) {
						if ($value['stock'] <= 0) continue;
						echo '<tr class="produit">
						<td>
							<img src="img/' . $value['img'] . '">
						</td>
						<td>' . $value['ref'] . '</div></td>
						<td>' . $value['description'] . '</td>
						<td>' . $value['prix'] . '€</td>'; ?>
						<td>
							<div class="nbr-select">
								<button type="button" class="btn-nbr" disabled>-</button>
								<input type="number" class="btn-input" value="0" min=0 max=<?php echo $value['stock'] ?> data-ref=<?php echo $value['ref'] ?>>
								<button type="button" class="btn-nbr">+</button>
							</div><br>
						<?php if ($_SESSION['user']) { ?>
							<button type="button" class="btn-add">Ajouter au panier</button>
						<?php } else { ?>
							<a href="connexion.php" type="button" class="btn-add">Ajouter au panier</a>
						<?php } ?>
						</td>
						<td class="td-stock" hidden><?php echo $value['stock'] ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</main>
	</div>

	<?php include 'php/footer.inc.php' ?>

	<div id="black-bg" style="visibility: hidden;">
		<img src="" alt="">
	</div>

</body>

</html>