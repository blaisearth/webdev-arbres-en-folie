<?php


/**
 * Connect to the database
 * 
 * @return mysqli Connection to the database
 */
function db_connect() {
    include "bddData.inc.php";
    if (!(isset($host) && isset($user) && isset($password))) {
        echo "Impossible de se connecter à la database: informations manquantes";
        exit(1);
    } if (!isset($port)) $port = 3306;
    $connexion = mysqli_connect($host, $user, $password, NULL, $port);
    if (!$connexion) {
        echo "La connexion a renvoyé une erreur avec le message : " . mysqli_connect_error();
        exit(1);
    }
    if (!mysqli_select_db($connexion, $db)) {
        echo "La connexion à la base a renvoyé une erreur de code " . mysqli_errno($connexion). " avec le message : " . mysqli_error($connexion);
        exit(1);
    };
    return $connexion;
};

/**
 * Check if a database connection is still open
 * 
 * @param mysqli $connexion Living connection to the database
 * 
 * @return Boolean If the connection is usable
 */
function db_check(mysqli $connexion) {
    if ($connexion->connect_errno) return false;
    if (!$connexion->ping()) return false;
    return true;
}

/**
 * Close a database connection
 * 
 * @param mysqli $connexion Living connection to the database
 * 
 */
function db_close(mysqli $connexion) {
    // check if the connection is still open
    if (db_check($connexion)) {
        $connexion->close();
    }
}

/**
 * Make a SQL statement to the database
 * 
 * Query can be any usual SQL statement (SELECT/INSERT/UPDATE/DELETE). If SELECT is used, the result will be the fetched rows.
 * Else, it will be a boolean indicating if the query was succesfully executed
 * 
 * @param ?mysqli $connexion Living connection to the database, NULL if we should create a temp one
 * @param string $query SQL query to execute, use '?' for arguments
 * @param string $types Parameters types, see https://www.php.net/manual/en/mysqli-stmt.bind-param.php#refsect1-mysqli-stmt.bind-param-parameters
 * @param mixed $params List of the query arguments to escape
 * 
 * @return array|boolean Array of one map per returned row for SELECT, or boolean indicating if all went well for others
 */
function db_query(?mysqli $connexion, string $query, string $types, ...$params) {
    // if user provided a connection: we use it
    if (isset($connexion)) $_cnx = $connexion;
    // else: we create it
    else $_cnx = db_connect();
    // prepare the request with provided query
    $stmt = $_cnx->prepare($query);
    // if statement couldn't be done
    if (!$stmt) {
        echo "Echec de la préparation de la requête avec le code ".$_cnx->errno." et le message : ".$_cnx->error."<br/>";
        exit();
    }
    // check if we use SELECT or another query
    $USING_SELECT = (substr($query, 0, 6) === "SELECT");
    // bind provided query parameters
    $stmt->bind_param($types, ...$params);
    // execute the query
    if ($stmt->execute()) {
        // if we're not making any SELECT, we can just say that it went well
        if (!$USING_SELECT) return true;
        $row = array(); // useless except for VSCode
        // prepare the result map from the statement metadata
        $meta = $stmt->result_metadata();
        while ($field = $meta->fetch_field())
        {
            $qu_params[] = &$row[$field->name];
        }
        // bind variables used to fetch the result
        call_user_func_array(array($stmt, 'bind_result'), $qu_params);
        // for each returned row
        while ($stmt->fetch()) {
            // for each variable in that row, we fillup the map
            foreach($row as $key => $val) {
                $c[$key] = $val;
            }
            // then append the map to the results array
            $result[] = $c;
        }
        return $result;
    } else {
        echo "Echec de la requête avec le code d’erreur " .mysqli_errno($_cnx)." et le message : " .mysqli_error($_cnx)."<br>";
        // if we're not making a SELECT query, we can just return false
        if (!$USING_SELECT) return false;
        exit();
    }
    // close the statement
    $stmt->close();
    // if connection was created by us: we close it
    if (!isset($connexion)) db_close($_cnx);
}

/**
 * Check if a username is already used
 * 
 * @param ?mysqli $connexion Living connection to the database, NULL if we should create a temp one
 * @param string $username The username to check
 * 
 * @return boolean True if the username already exists
 */
function check_username(?mysqli $connexion, string $username) {
    $query = 'SELECT 1 FROM users WHERE username = ?';
    $res = db_query($connexion, $query, "s", $username);
    return (count($res) >= 1);
}

/**
 * Get a user from its username or email
 * 
 * @param ?mysqli $connexion Living connection to the database, NULL if we should create a temp one
 * @param string $usernameOrEmail The username or email of the user to get
 */
function get_user(?mysqli $connexion, string $usernameOrEmail) {
    $query = 'SELECT * FROM users WHERE username = ? OR email = ?';
    $res = db_query($connexion, $query, "ss", $usernameOrEmail, $usernameOrEmail);
    return $res[0];
}

/**
 * Add a new user to the database
 * 
 * If the operation fails, a message will be printed and "false" will be returned
 * 
 * @param ?mysqli $connexion Living connection to the database, NULL if we should create a temp one
 * @param array $data The map containing every user info. At least username, firstname, lastname and password should be provided
 * 
 * @return boolean If the user was succesfully added
 */
function add_user(?mysqli $connexion, array $data) {
    if (!(isset($data['username']) && isset($data['firstname']) && isset($data['lastname']) && isset($data['password']))) {
        echo "Impossible d'ajouter l'utilisateur : champs manquants";
        exit(1);
    }
    $columns = array('username', 'firstname', 'lastname', 'password', 'street', 'postal', 'country', 'email', 'birthdate', 'genre', 'work');
    $keys = array(); $values = array(); $types = '';
    foreach ($columns as $col) {
        if ($val = $data[$col]) {
            $keys[] = '`'.$col.'`';
            $values[] = $val;
            if ($col === "postal") $types .= 'i';
            else $types .= 's';
        }
    }
    $placeholders = join(', ', array_fill(0, count($values), '?'));
    $query = 'INSERT INTO users ('.join(",", $keys).') VALUES ('.$placeholders.')';
    return db_query($connexion, $query, $types, ...$values);
}

/**
 * Get a product from its reference
 * 
 * @param ?mysqli $connexion Living connection to the database, NULL if we should create a temp one
 * @param string $red The reference of the product to get
 */
function get_product(?mysqli $connexion, string $ref) {
    // if ref can be casted as interger, it's an ID
    if ((int) $ref) {
        $query = 'SELECT * FROM produits WHERE id = ?';
        $res = db_query($connexion, $query, "i", $ref);
    } else {
        $query = 'SELECT * FROM produits WHERE ref = ?';
        $res = db_query($connexion, $query, "s", $ref);
    }
    return $res[0];
}

/**
 * Remove some stock from a product
 * 
 * @param ?mysqli $connexion Living connection to the database, NULL if we should create a temp one
 * @param int $id ID of the product to edit
 * @param int $qty Quantity to remove
 * 
 * @return boolean If all went well
 */
function remove_stock(?mysqli $connexion, int $id, int $qty) {
    $query = "UPDATE produits SET stock = stock - ? WHERE id = ?";
    return db_query($connexion, $query, "ii", $qty, $id);
}

/**
 * Get a category from a given name
 * 
 * @param ?mysqli $connexion Living connection to the database, NULL if we should create a temp one
 * @param string $name Name of the category to fetch
 * 
 * @return int ID of the category
 */
function get_category_id(?mysqli $connexion, string $name) {
    $query = 'SELECT id FROM categories WHERE nom = ?';
    $res = db_query($connexion, $query, 's', $name);
    return ($res[0]['id']);
}

/**
 * Get the products from a category
 * 
 * @param ?mysqli $connexion Living connection to the database, NULL if we should create a temp one
 * @param int $categID ID of the category to look for
 * 
 * @return array List of products
 */
function get_products(?mysqli $connexion, int $categID) {
    $query = 'SELECT * FROM produits WHERE categorie = ?';
    return db_query($connexion, $query, 'i', $categID);
}

/**
 * Get the list of existing categories
 * 
 * @param ?mysqli $connexion Living connection to the database, NULL if we should create a temp one
 * 
 * @return array List of category names
 */
function get_categories(?mysqli $connexion) {
    $query = 'SELECT nom FROM categories ORDER BY id';
    $res = db_query($connexion, $query, '');
    return array_map(function($r) {return $r['nom'];}, $res);
}