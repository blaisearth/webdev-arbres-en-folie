<?php

// init session
session_start();
include "bdd/bdd.inc.php";

// Takes raw data from the request
$json = file_get_contents('php://input');
// Converts it into a PHP object
$data = json_decode($json);

// if user is not logged in: abort
if (!isset($_SESSION['user'])) {
    http_response_code(401);
    echo "You need to log in first";
    exit();
}

$cnx = db_connect();

$ref_table = array();

// check each item
foreach ($data as $item) {
    // if a param is missing: abort
    if (!( isset($item->ref) && isset($item->qty) )) {
        http_response_code(400);
        echo "Missing 'ref' or 'qty' parameter";
        db_close($cnx);
        exit();
    }

    // get product from file
    $product = get_product($cnx, $item->ref);

    // if ref doesn't exist: abort
    if (!isset($product)) {
        http_response_code(404);
        echo "Unknown product";
        db_close($cnx);
        exit();
    }

    $ref_table[$item->ref] = $product['id'];

    // if stock isn't enough: abort
    if ($product['stock'] < $item->qty) {
        
        http_response_code(400);
        echo "Too many items requested! Max is ".$product['stock'];
        db_close($cnx);
        exit();
    }
}

unset($item);

// remove stock for each item
foreach ($data as $item) {
    var_dump($item);
    if (!remove_stock($cnx, $ref_table[$item->ref], $item->qty)) {
        http_response_code(400);
        echo "Oops, something went wrong";
        db_close($cnx);
        exit();
    }
}

http_response_code(200);
echo "OK";

db_close($cnx);

// reset cart
$_SESSION['panier'] = array();