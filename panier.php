<?php
session_start();

include "php/varSession.inc.php";

$total_price = 0.0;

// prepare products if needed
if ($_SESSION['panier']) {
    $cnx = db_connect();
    $products = array();
    foreach ($_SESSION['panier'] as $item) {
        if ($item['qty'] <= 0) continue;
        if ($t = get_product($cnx, $item['id'])) {
            $t['qty'] = $item['qty'];
            $products[] = $t;
            $total_price += $t['prix']*$t['qty'];
        }
    }
    db_close($cnx);
}
?>

<!DOCTYPE html>
<html <?php echo class_theme(); ?>>

<head>
    <title>Mon panier</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/master.css" />
    <link rel="stylesheet" type="text/css" href="css/contact.css">
    <link rel="stylesheet" type="text/css" href="css/product.css" />
    <link rel="stylesheet" type="text/css" href="css/panier.css" />
	<script type="text/javascript" src="js/panier.js"></script>
    <script src="js/main.js"></script>
</head>

<body>

    <?php include 'php/header.inc.php'; ?>

    <div id="main">
        <?php include 'php/menu.inc.php'; ?>
        <main>
            <h1 id="texte3"> Votre panier </h1>

            <?php
            if (!isset($products)) { ?>
                <div id="erreur">Votre panier est vide !</div>
            <?php } else { ?>

            <button id="accept-cmd">Passer la commande - <?php echo $total_price ?>€</button>
            <table>
				<tbody>
					<tr id="tableheader">
						<td>Aperçu du produit</td>
						<td>Nom</td>
						<td>Prix</td>
						<td>Quantité</td>
					</tr>

                    <?php
                    $cnx = db_connect();
                    foreach ($products as $product) {
                        if (!isset($product)) continue;
                        echo '<tr class="produit">
						<td>
							<img src="img/' . $product['img'] . '">
						</td>
						<td>' . ucfirst($product['nom']) . '</div></td>
						<td>' . $product['prix'] . '€</td>
						<td>
							<div class="nbr-select">
								<button type="button" class="btn-nbr">-</button>
								<input type="number" class="btn-input" value="'.$product['qty'].'" min=0 max=' . $product['stock'] . ' data-ref="' . $product['ref'] . '">
								<button type="button" class="btn-nbr">+</button>
							</div><br>
						</td>
					</tr>';
                    }
                    ?>
				</tbody>
			</table>
            <?php } ?>
        </main>
    </div>

    <?php include 'php/footer.inc.php' ?>

</body>

</html>