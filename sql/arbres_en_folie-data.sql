-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : mer. 14 avr. 2021 à 08:16
-- Version du serveur :  5.7.32
-- Version de PHP : 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données : `arbres_en_folie`
--

USE arbres_en_folie;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`) VALUES
(2, 'atypiques'),
(0, 'classiques'),
(1, 'fruitiers');

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id`, `categorie`, `ref`, `nom`, `description`, `stock`, `prix`, `img`) VALUES
(1, 0, 'AC01', 'sapin', 'Sapin de 1.5m', 20, 50, 'classique/sapin.jpg'),
(2, 0, 'AC02', 'bouleau', 'Bouleau de 2m', 20, 90, 'classique/bouleau.jpg'),
(3, 0, 'AC03', 'érable', 'Érable de 2m', 15, 110, 'classique/erable.jpg'),
(4, 0, 'AC04', 'hêtre', 'Hêtre de 80cm', 20, 20, 'classique/hetre.jpg'),
(5, 0, 'AC05', 'peuplier', 'Lot de deux peupliers 1.5m et 2m', 20, 40, 'classique/peuplier.jpg'),
(6, 1, 'AF01', 'pommier', 'Pommier de 40cm', 18, 24, 'fruitier/pommier.png'),
(7, 1, 'AF02', 'poirier', 'Poirier de 45cm', 20, 7, 'fruitier/poirier.png'),
(8, 1, 'AF03', 'prunier', 'Petit prunier de 50cm', 17, 13, 'fruitier/prunier.png'),
(9, 1, 'AF04', 'cerisier', 'Cerisier format éco de 40cm', 10, 10, 'fruitier/cerisier.png'),
(10, 1, 'AF05', 'pêchier', 'Pêchier de 140cm', 20, 90, 'fruitier/pechier.png'),
(11, 2, 'AA01', 'bonsaï', 'Bonsaï de 333mm', 20, 103, 'atypique/bonsai.png'),
(12, 2, 'AA02', 'prunus serrulata', 'Prunus serrulata de 1m', 20, 77, 'atypique/prunus_serrulata.jpg'),
(13, 2, 'AA03', 'saule pleureur', 'Saule pleureur de 1.5m', 20, 60, 'atypique/saule_pleureur.jpg'),
(14, 2, 'AA04', 'palmier', 'Palmier mini de 60cm', 20, 70, 'atypique/palmier.png'),
(15, 2, 'AA05', '4 mini cactus', 'Lot de 4 mini cactus environ 10cm chacun', 20, 15, 'atypique/cactus.jpg');

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`username`, `firstname`, `lastname`, `password`, `street`, `postal`, `country`, `email`, `birthdate`, `genre`, `work`) VALUES
('alting22', 'Corette', 'Quiron', 'eng@quiron255', '136 boulevard Bryas', 60175, 'France', NULL, NULL, NULL, NULL),
('blaisearth', 'Arthur', 'Blaise', 'aJC59zxcSg2v8Wj', '20 rue du Parc', 95000, 'France', 'arthur.blaise.23@gmail.com', '2000-11-23', 'Homme', 'Agriculteur'),
('burnice', 'Alexander', 'Binion', 'aed0Ahw1oz', '10 rue La Boétie', 75011, 'France', NULL, NULL, NULL, NULL),
('hmob101', 'Hélène', 'Mobley', 'aetuopmkhfqw', '101 Quai des Belges', 13056, 'France', NULL, NULL, 'Femme', NULL),
('will247', 'Williams', 'Ken', '12345678', '41 rue Adolphe Wurtz', 92060, 'France', NULL, NULL, 'Homme', NULL);
