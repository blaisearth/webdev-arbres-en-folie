-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : mer. 14 avr. 2021 à 07:22
-- Version du serveur :  5.7.32
-- Version de PHP : 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données : `arbres_en_folie`
--
CREATE DATABASE IF NOT EXISTS `arbres_en_folie` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `arbres_en_folie`;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--
-- Création : mer. 14 avr. 2021 à 07:06
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `nom` varchar(30) NOT NULL
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--
-- Création : mer. 14 avr. 2021 à 07:14
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE `produits` (
  `id` int(11) UNSIGNED NOT NULL,
  `categorie` int(11) UNSIGNED NOT NULL,
  `ref` varchar(5) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `description` varchar(120) NOT NULL,
  `stock` int(10) UNSIGNED NOT NULL,
  `prix` double UNSIGNED NOT NULL,
  `img` varchar(40) NOT NULL
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--
-- Création : mer. 14 avr. 2021 à 07:11
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `username` varchar(32) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(80) NOT NULL,
  `street` text,
  `postal` mediumint(9) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `genre` varchar(20) DEFAULT NULL,
  `work` varchar(32) DEFAULT NULL
) DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nom` (`nom`);

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categorie` (`categorie`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `produits`
--
ALTER TABLE `produits`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `produits_ibfk_1` FOREIGN KEY (`categorie`) REFERENCES `categories` (`id`);
