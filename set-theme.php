<?php

// init session
session_start();

// Takes raw data from the request
$json = file_get_contents('php://input');
// Converts it into a PHP object
$data = json_decode($json);

if (!isset($data->theme)) {
    http_response_code(400);
    echo "Missing 'theme' parameter";
    exit();
}

$theme = $data->theme;

if ($theme !== "dark" && $theme !== "light") {
    http_response_code(400);
    echo "Invalid theme: use either dark or light";
    exit();
}

$_SESSION['theme'] = $theme;

http_response_code(200);
echo "OK";
?>