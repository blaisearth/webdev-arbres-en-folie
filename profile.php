<?php
session_start();

include "php/varSession.inc.php";

// if user isn't connected: redirect to home page
if (!isset($_SESSION['user'])) {
    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    header("Location: http://$host$uri/connexion.php");
    exit();
}

?>

<!DOCTYPE html>
<html <?php echo class_theme(); ?>>

<head>
    <title>Mon compte</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/master.css" />
    <link rel="stylesheet" type="text/css" href="css/contact.css">
    <link rel="stylesheet" type="text/css" href="css/profile.css">
    <script src="js/main.js"></script>
</head>

<body>

    <?php include 'php/header.inc.php'; ?>

    <div id="main">
        <?php include 'php/menu.inc.php'; ?>
        <main>

            <form method="POST">

                <h2> Vos informations </h2>

                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <div>Pseudo</div> <?php echo $_SESSION['user']['username']; ?>
                    </div>

                    <div class="form-group col-lg-4">
                        <div>Mot de passe</div> <?php echo $_SESSION['user']['password']; ?>
                    </div>
                    <div class="form-group col-lg-4">
                        <div>Adresse email</div> <?php echo $_SESSION['user']['email'] ?: "Inconnue"; ?>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <div>Nom</div> <?php echo $_SESSION['user']['lastname']; ?>
                    </div>

                    <div class="form-group col-lg-6">
                        <div>Prénom</div> <?php echo $_SESSION['user']['firstname']; ?>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-lg-4">
                        <div>Genre</div> <?php echo $_SESSION['user']['genre'] ?: "Inconnu"; ?>
                    </div>

                    <div class="form-group col-lg-4">
                        <div>Date de naissance</div> <?php echo $_SESSION['user']['birthdate'] ?: "Inconnue"; ?>
                    </div>

                    <div class="form-group col-lg-4">
                        <div>Emploi</div> <?php echo $_SESSION['user']['work'] ?: "Inconnu"; ?>
                    </div>
                </div>

                <div class="form-group">
                    <div>Adresse</div> <?php echo $_SESSION['user']['address'] ?: "Inconnue"; ?>
                </div>

            </form>

            <a id="logout" href="logout.php">Se déconnecter</a>

        </main>
    </div>

    <?php include 'php/footer.inc.php' ?>

</body>

</html>