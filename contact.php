<?php
session_start();

include "php/varSession.inc.php";

$lastname = $_POST['inputName'] ?: $_SESSION['user']['lastname'];
$firstname = $_POST['inputFirstname'] ?: $_SESSION['user']['firstname'];
$email = $_POST["inputEmail"] ?: $_SESSION['user']['email'];
$birthdate = $_POST["inputBirthdate"] ?: $_SESSION['user']['birthdate'];
$genre = $_POST["genre"];
$work = $_POST["inputWork"];
$topic = $_POST["inputTopic"];
$message = $_POST["inputMessage"];

$email_sent = false;

if (isset($message)) {
	if (empty($lastname))
		$alerte = 'Nom laissé vide';
	elseif (empty($email))
		$alerte = 'Email laissé vide';
	elseif (empty($firstname))
		$alerte = 'Prénom laissé vide';
	elseif (empty($birthdate))
		$alerte = 'Date de naissance laissé vide';
	elseif (empty($topic))
		$alerte = 'Sujet laissé vide';
	elseif (empty($message))
		$alerte = 'Message laissé vide';
	else {
		$message = wordwrap($message, 70, "\r\n");
		mail('dossantosn@eisti.eu', $topic, $message);
		$email_sent = true;
	}
}
?>

<!DOCTYPE html>
<html <?php echo class_theme(); ?>>

<head>
	<title>Arbres en folie</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/master.css" />
	<link rel="stylesheet" type="text/css" href="css/contact.css">
	<script src="js/main.js"></script>
</head>

<body>

	<?php include 'php/header.inc.php'; ?>

	<div id="main">
		<?php include 'php/menu.inc.php'; ?>
		<main>
			<?php if ($alerte) echo "<div class='erreur'>" . $alerte . "</div>";
			if ($email_sent) echo "<div class='confirm'>Mail envoyé !</div>"; ?>
			<form method="POST">
				<div class="form-row">
					<div class="form-group col-lg-6">
						<label for="inputName">Nom</label>
						<input type="text" class="form-control" id="inputName" name="inputName" autocomplete="family-name" placeholder="Entrez votre nom" <?php echo 'value="'.htmlentities($lastname).'"'; if ($_SESSION['user']['lastname']) echo 'disabled' ?>  required>
						<label for="inputFirstname">Prénom</label>
						<input type="text" class="form-control" id="inputFirstname" name="inputFirstname" autocomplete="given-name" placeholder="Entrez votre prénom" <?php echo 'value="'.htmlentities($firstname).'"'; if ($_SESSION['user']['firstname']) echo 'disabled' ?> required>
					</div>
					<div class="form-group col-lg-6">
					<label for="inputEmail">Email</label>
						<input type="email" class="form-control" id="inputEmail" autocomplete="email" name="inputEmail" placeholder="Entrez votre Email" <?php echo 'value="'.htmlentities($email).'"'; if ($_SESSION['user']['email']) echo 'disabled' ?> required>
						<label for="inputBirthdate">Date de naissance</label>
						<input type="date" class="form-control" id="inputBirthdate" name="inputBirthdate" min="1920-01-01" max="2020-12-31" autocomplete="bday" <?php echo 'value="'.htmlentities($birthdate).'"'; if ($_SESSION['user']['birthdate']) echo 'disabled' ?> required>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-lg-6">
						<label class="form-check-label" for="inputGenre">Genre</label>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="genre" id="genreM" value="Homme" required>
							<label class="form-check-label" for="genreM">
								Homme
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="genre" id="genreF" value="Femme" required>
							<label class="form-check-label" for="genreF">
								Femme
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="genre" id="genreA" value="Autre/Ne souhaite pas se prononcer" required>
							<label class="form-check-label" for="genreA">
								Autre/Ne souhaite pas se prononcer
							</label>
						</div>
					</div>
					<div class="form-group col-lg-6">
						<label for="inputWork">Métier</label>
						<select class="form-control" id="inputWork" name="inputWork" required>
							<?php include('php/work_types.inc.php') ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputTopic">Sujet</label>
					<input type="text" class="form-control" id="inputTopic" name="inputTopic" placeholder="Objet de votre email" minlength="10" value="<?php echo htmlentities($topic) ?>" required>
				</div>
				<div class="form-group">
					<label for="inputMessage">Message</label>
					<textarea class="form-control" id="inputMessage" rows="5" placeholder="Détaillez ici votre message" name="inputMessage" minlength="50" maxlength="5000" value="<?php echo htmlentities($message) ?>" required></textarea>
				</div>
				<button type="submit" class="btn green">Envoyer</button>
			</form>
		</main>
	</div>

	<?php include 'php/footer.inc.php' ?>

</body>

</html>