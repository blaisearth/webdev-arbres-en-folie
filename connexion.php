<?php
session_start();

include "php/varSession.inc.php";

// redirect if user is already logged in
if (isset($_SESSION['user'])) {
    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    header("Location: http://$host$uri/profile.php");
    exit();
}

$password_wrong = false;
$username_wrong = false;

if (isset($_POST["username"]) && isset($_POST["password"])) {
    $potential_user = get_user(NULL, $_POST["username"]);

    if ($potential_user) {
        // username exists
        if ($potential_user['password'] === $_POST["password"]) {
            // password matches
            // save user in session
            $_SESSION["user"] = $potential_user;
            // redirect to home page
            $host  = $_SERVER['HTTP_HOST'];
            $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
            header("Location: http://$host$uri/");
            exit();
        } else {
            $password_wrong = true;
        }
    } else {
        $username_wrong = true;
    }
}
?>

<!DOCTYPE html>
<html <?php echo class_theme(); ?>>

<head>
    <title>Arbres en folie - Connexion</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/master.css" />
    <link rel="stylesheet" type="text/css" href="css/contact.css">
    <script src="js/main.js"></script>
    <style>
    #error {
        color: red;
        margin: 2%;
    }
    form {
        display: flex;
        flex-direction: column;
        min-width: 40%;
    }
    form a {
        text-align: center;
        margin-top: 5%;
        font-size: small;
    }
    </style>
</head>
<body>

    <?php include 'php/header.inc.php'; ?>

    <div id="main">
        <?php include 'php/menu.inc.php'; ?>
        <main>
        <?php
            if ($username_wrong) echo "<div id='error'>Utilisateur inconnu</div>";
            else if ($password_wrong) echo "<div id='error'>Mot de passe invalide</div>";
        ?>
            <form method="POST">

                <div class="form-group">
                    <label for="username">Identifiant</label>
                    <input type="text" class="form-control" id="username" name="username" autocomplete="username" placeholder="Entrez votre identifiant" minlength="5" value="<?php echo $_POST["username"] ?>" required>
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" class="form-control" id="password" name="password" autocomplete="current-password" placeholder="Entrez votre mot de passe" minlength="8" required>
                </div>

                <button type="submit" class="btn green">Se connecter</button>
                <a href="signup.php">Créer un compte</a>

            </form>
        </main>
    </div>

    <?php include 'php/footer.inc.php' ?>

</body>

</html>