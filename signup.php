<?php
session_start();

include "php/varSession.inc.php";

// redirect if user is already logged in
if (isset($_SESSION['user'])) {
    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    header("Location: http://$host$uri/profile.php");
    exit();
}

include 'bdd/bdd.inc.php';

$lastname = $_POST['inputName'];
$firstname = $_POST['inputFirstname'];
$email = $_POST["inputEmail"];
$birthdate = $_POST["inputBirthdate"];
$genre = $_POST["genre"];
$work = $_POST["inputWork"];
$username = $_POST["inputUsername"];
$password = $_POST["inputPassword"];
$street = $_POST['inputStreet'];
$postal = $_POST["inputPostal"];
$country = $_POST["inputCountry"];

$email_sent = false;

if (isset($firstname)) {
	$cnx = db_connect();
	if (empty($lastname))
		$alerte = 'Nom laissé vide';
	elseif (empty($email))
		$alerte = 'Email laissé vide';
	elseif (empty($firstname))
		$alerte = 'Prénom laissé vide';
	elseif (empty($birthdate))
		$alerte = 'Date de naissance laissé vide';
    elseif (empty($password))
		$alerte = 'Mot de passe laissé vide';
    elseif (empty($street))
		$alerte = 'Adresse laissée vide';
    elseif (empty($postal))
		$alerte = 'Code postal laissé vide';
    elseif (empty($country))
		$alerte = 'Pays laissé vide';
    elseif(check_username($cnx, $username)) {
        $alerte = "Nom d'utilisateur déjà pris";
    }
	else {
		$user = array(
			"username" => $username,
			"firstname" => $firstname,
			"lastname" => $lastname,
			"password" => $password,
			"address" => $street.', '.$postal.' '.$country,
			"street" => $street,
			"postal" => $postal,
			"country" => $country,
			"email" => $email,
			"birthdate" => $birthdate,
			"genre" => $genre,
			"work" => $work
		);
		if (add_user($cnx, $user)) {
			db_close($cnx);
			$_SESSION['user'] = $user;
			$host  = $_SERVER['HTTP_HOST'];
	    	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	    	header("Location: http://$host$uri/");
	    	exit();
		} else {
			$alerte = "Oups, quelque chose s'est mal passé lors de votre création de compte... Réessayez plus tard ou contactez-nous !";
		}
	}
	db_close($cnx);
}
?>

<!DOCTYPE html>
<html <?php echo class_theme(); ?>>

<head>
	<title>Arbres en folie - Inscription</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/master.css" />
	<link rel="stylesheet" type="text/css" href="css/contact.css">
	<script src="js/main.js"></script>
</head>

<body>

	<?php include 'php/header.inc.php'; ?>

	<div id="main">
		<?php include 'php/menu.inc.php'; ?>
		<main>
			<?php if ($alerte) echo "<div class='erreur'>" . $alerte . "</div>"; ?>
			<form method="POST">
				<div class="form-row">
					<div class="form-group col-lg-6">
						<label for="inputName">Nom</label>
						<input type="text" class="form-control" id="inputName" name="inputName" autocomplete="family-name" placeholder="Entrez votre nom" <?php echo 'value="'.htmlentities($lastname).'"' ?>  required>
                        <label for="inputFirstname">Prénom</label>
						<input type="text" class="form-control" id="inputFirstname" name="inputFirstname" autocomplete="given-name" placeholder="Entrez votre prénom" <?php echo 'value="'.htmlentities($firstname).'"' ?> required>
					</div>
					<div class="form-group col-lg-6">
                        <label for="inputEmail">Email</label>
						<input type="email" class="form-control" id="inputEmail" autocomplete="email" name="inputEmail" placeholder="Entrez votre Email" value="<?php echo htmlentities($email) ?>" required>
						<label for="inputBirthdate">Date de naissance</label>
						<input type="date" class="form-control" id="inputBirthdate" name="inputBirthdate" min="1920-01-01" max="2020-12-31" autocomplete="bday" value="<?php echo htmlentities($birthdate) ?>" required>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-lg-6">
						<label class="form-check-label" for="inputGenre">Genre</label>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="genre" id="genreM" value="Homme">
							<label class="form-check-label" for="genreM">
								Homme
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="genre" id="genreF" value="Femme">
							<label class="form-check-label" for="genreF">
								Femme
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="genre" id="genreA" value="Autre">
							<label class="form-check-label" for="genreA">
								Autre/Ne souhaite pas se prononcer
							</label>
						</div>
					</div>
					<div class="form-group col-lg-6">
						<label for="inputWork">Métier</label>
						<select class="form-control" id="inputWork" name="inputWork">
							<?php include('php/work_types.inc.php') ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputStreet">Adresse</label>
					<input type="text" class="form-control" id="inputStreet" name="inputStreet" autocomplete="street-address" placeholder="Ex: 20 rue du Parc" value="<?php echo htmlentities($street) ?>" required>
				</div>
				<div class="form-row">
                    <div class="form-group col-lg-6">
                    	<label for="inputPostal">Code postal</label>
						<input type="text" class="form-control" id="inputPostal" name="inputPostal" autocomplete="postal-code" placeholder="Ex: 95000" value="<?php echo htmlentities($postal) ?>" maxlength="5" required>
					</div>
					<div class="form-group col-lg-6">
						<label for="inputCountry">Pays</label>
						<input type="text" class="form-control" id="inputCountry" name="inputCountry" autocomplete="country-name" placeholder="Ex: France" value="<?php echo htmlentities($country) ?>" required>
                    </div>
                </div>
				<div class="form-row">
					<div class="form-group col-lg-6">
                        <label for="inputUsername">Nom d'utilisateur</label>
						<input type="text" class="form-control" id="inputUsername" autocomplete="username" name="inputUsername" placeholder="Votre identifiant de connexion" minlength="5" maxlength="20" value="<?php echo htmlentities($username) ?>" required>
					</div>
					<div class="form-group col-lg-6">
						<label for="inputPassword">Mot de passe</label>
						<input type="password" class="form-control" id="inputPassword" autocomplete="new-password" name="inputPassword" placeholder="Entrez un mot de passe" minlength="8" maxlength="30" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).+" title="Doit contenir au moins un chiffre et une lettre majuscule et minuscule, et au moins 8 caractères" required>
					</div>
				</div>
				<button type="submit" class="btn green">Créer son compte</button>
			</form>
		</main>
	</div>

	<?php include 'php/footer.inc.php' ?>

</body>

</html>