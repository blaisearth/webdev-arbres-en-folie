function moins(event) {
    // get input field
    let elem = event.target.parentNode.getElementsByTagName('input')[0];
    // get current value as integer
    const value = parseInt(elem.value);
    if (value > elem.min) {
        // if value is higher than max, we lower it to max
        elem.value = Math.min(parseInt(elem.max), value - 1);
    }
    updateButtons(elem);
}

function plus(event) {
    // get input field
    let elem = event.target.parentNode.getElementsByTagName('input')[0];
    // get current value as integer
    const value = parseInt(elem.value);
    if (value < elem.max) {
        // if value is lower than min, we raise it to min
        elem.value = Math.max(parseInt(elem.min), value + 1);
    }
    updateButtons(elem);
}

// enable/disable the min and max buttons
function updateButtons(elem) {
    let min = elem.parentNode.getElementsByTagName('button')[0];
    let max = elem.parentNode.getElementsByTagName('button')[1];
    const value = parseInt(elem.value);
    min.disabled = (value <= elem.min);
    max.disabled = (value >= elem.max)
}

// validate the cart
function validate(event) {
    const inputs = document.querySelectorAll("input.btn-input");
    let command = Array.from(inputs, e => {return {
        ref: e.dataset['ref'],
        qty: parseInt(e.value)
    }});
    fetch("buy.php", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(command)
    }).then(res => {
        if (res.ok) {
            alert("Votre commande a bien été passée !");
        } else {
            alert("Oups, quelque chose s'est mal passé !");
        }
    })
}

window.addEventListener("load", function () {
    // add events for decrement/increment buttons
    btns = document.getElementsByClassName('btn-nbr');
    Array.prototype.forEach.call(btns, e => {
        if (e.innerText == "-") {
            e.addEventListener('click', moins);
        } else if (e.innerText == '+') {
            e.addEventListener('click', plus);
        }
    })
    // add "validate cart" btn
    document.getElementById("accept-cmd").addEventListener("click", validate);
})