const STOCK_DISPLAY = "Afficher le stock";
const STOCK_HIDE = "Masquer le stock";
const HOVER_COEF = 1.3;

function moins(event) {
    // get input field
    let elem = event.target.parentNode.getElementsByTagName('input')[0];
    // get current value as integer
    const value = parseInt(elem.value);
    if (value > elem.min) {
        // if value is higher than max, we lower it to max
        elem.value = Math.min(parseInt(elem.max), value - 1);
    }
    updateButtons(elem);
}

function plus(event) {
    // get input field
    let elem = event.target.parentNode.getElementsByTagName('input')[0];
    // get current value as integer
    const value = parseInt(elem.value);
    if (value < elem.max) {
        // if value is lower than min, we raise it to min
        elem.value = Math.max(parseInt(elem.min), value + 1);
    }
    updateButtons(elem);
}

// enable/disable the min and max buttons
function updateButtons(elem) {
    let min = elem.parentNode.getElementsByTagName('button')[0];
    let max = elem.parentNode.getElementsByTagName('button')[1];
    const value = parseInt(elem.value);
    min.disabled = (value <= elem.min);
    max.disabled = (value >= elem.max)
}

function toggleStocks(event) {
    const stocks = document.getElementsByClassName('td-stock');
    // if we should display the column
    if (event.target.innerText == STOCK_DISPLAY) {
        // update text
        event.target.innerText = STOCK_HIDE;
        // unhide column
        Array.prototype.forEach.call(stocks, e => {
            e.hidden = false;
        });
    } else {
        // update text
        event.target.innerText = STOCK_DISPLAY;
        // hide column
        Array.prototype.forEach.call(stocks, e => {
            e.hidden = true;
        });
    }
}

// display in large when clicking on an image
function clickImg(event) {
    let elem = event.target;
    let bg_screen = document.getElementById("black-bg");
    bg_screen.style.visibility = "visible";
    let bg_img = bg_screen.getElementsByTagName('img')[0];
    bg_img.src = elem.src;
    bg_img.alt = elem.alt;
}

function unclickImg(event) {
    let elem = event.target;
    // if we clicked on the img -> ignore
    if (elem.tagName == "IMG") return;
    elem.style.visibility = "hidden";
}

// add a product to the cart
function addproduct(event) {
    const parent = event.target.parentNode;
    const input = parent.getElementsByTagName("input")[0];
    const value = parseInt(input.value);
    if (value <= 0) return;
    const ref = input.dataset['ref'];
    fetch("add-panier.php", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ref: ref, qty: value})
    }).then(res => {
        if (res.ok) {
            alert("Produit ajouté à votre panier !");
        } else {
            alert("Oups, quelque chose s'est mal passé !");
        }
    })
}


window.addEventListener("load", function () {
    // add the function to hide/show stocks
    document.getElementById("toggler").addEventListener('click', toggleStocks);
    document.getElementById("toggler").innerText = STOCK_DISPLAY;
    // add events for decrement/increment buttons
    btns = document.getElementsByClassName('btn-nbr');
    Array.prototype.forEach.call(btns, e => {
        if (e.innerText == "-") {
            e.addEventListener('click', moins);
        } else if (e.innerText == '+') {
            e.addEventListener('click', plus);
        }
    })
    // add img zoom on hover + on click
    imgs = document.querySelectorAll('.produit img');
    Array.prototype.forEach.call(imgs, e => {
        e.addEventListener('click', clickImg);
    })
    // hide big img preview when clicking somewhere else
    document.getElementById("black-bg").addEventListener("click", unclickImg);
    // add to cart when clicking
    add_btns = document.querySelectorAll('button.btn-add');
    Array.prototype.forEach.call(add_btns, e => {
        e.addEventListener("click", addproduct);
    })
})