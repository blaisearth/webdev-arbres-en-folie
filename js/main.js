function get_theme(use_local = true) {
    if (use_local) {
        const current_theme = document.documentElement.className.match(/(?<=theme-)([a-z]+)/g);
        if (current_theme) return current_theme[0];
    }
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        return 'dark';
    } return 'light';
}

function set_theme(theme) {
    console.debug("New theme:", theme)
    document.documentElement.className = 'theme-' + theme;
    fetch("set-theme.php", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({theme: theme})
    })
}

function toggle_theme() {
    curr_theme = get_theme(true)
    if (curr_theme == 'dark') {
        set_theme('light');
    } else {
        set_theme('dark');
    }
}

window.addEventListener("load", function () {
    document.getElementById('btn-theme')?.addEventListener('click', toggle_theme);
    if (!['theme-light', 'theme-dark'].includes(document.documentElement.className)) {
        const th = get_theme(true);
        console.debug("Setting theme according to local one: "+th)
        document.documentElement.className = 'theme-' + th;
    } else {
        console.debug("Theme set by server to "+document.documentElement.className)
    }
})