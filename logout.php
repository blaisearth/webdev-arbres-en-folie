<?php
	session_start();
	$_SESSION = array();
	session_destroy();
	// redirection vers l'accueil
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	header("Location: http://$host$uri/");
?>