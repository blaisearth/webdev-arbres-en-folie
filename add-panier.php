<?php

// init session
session_start();
include "bdd/bdd.inc.php";

// Takes raw data from the request
$json = file_get_contents('php://input');
// Converts it into a PHP object
$data = json_decode($json);

// if user is not logged in: abort
if (!isset($_SESSION['user'])) {
    http_response_code(401);
    echo "You need to log in first";
    exit();
}

// if a param is missing: abort
if (!( isset($data->ref) && isset($data->qty) )) {
    http_response_code(400);
    echo "Missing 'ref' or 'qty' parameter";
    exit();
}

// get product from file
$product = get_product(NULL, $data->ref);

// if ref doesn't exist: abort
if (!isset($product)) {
    http_response_code(404);
    echo "Unknown product";
    exit();
}

// if stock isn't enough: abort
if ($product['stock'] < $data->qty) {
    http_response_code(400);
    echo "Too many items requested! Max is ".$product['stock'];
    exit();
}

// check if product is already in cart
$key = array_search($product['id'], array_column($_SESSION['panier'], 'id'));
if ($key !== false) {
    $_SESSION['panier'][$key]['qty'] += $data->qty;
} else {
    // else push it
    $_SESSION['panier'][] = array(
        "id" => $product['id'],
        "qty" => $data->qty
    );
}


http_response_code(200);
echo "OK.";