<?php
session_start();

include "php/varSession.inc.php";
?>

<!DOCTYPE html>
<html <?php echo class_theme(); ?>>

<head>
	<title>Arbres en folie</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/master.css" />
	<script src="js/main.js"></script>
	<style>
		#texte2 {
			font-weight: bold;
			margin-top: 5%;
			text-align: center;
		}
	</style>
</head>

<body>

	<?php include 'php/header.inc.php'; ?>

	<div id="main">
		<?php include 'php/menu.inc.php'; ?>
		<main>
			<h2>Pour un monde qui respire</h2>
			<img src="img/logo.png" width="250px" height="210px"><br><br>

			<div id="texte2">
				Appelez notre service commercial au <a href="tel:+33501020304">05.01.02.03.04</a> pour recevoir un bon
				de commande
			</div>
		</main>
	</div>

	<?php include 'php/footer.inc.php' ?>

</body>

</html>