<?php
    if (!function_exists("get_categories")) {
        include "bdd/bdd.inc.php";
    }

    // Préparation du panier
    if (!isset($_SESSION['panier'])) $_SESSION['panier'] = array();

    // Récupération des catégories
    if (!isset($_SESSION['categories'])) $_SESSION['categories'] = get_categories(NULL);

    /**
     * Get the theme used by the current user, if any
     * 
     * @return string html theme class attribute to apply
     */
    function class_theme() {
        if (isset($_SESSION['theme'])) return 'class="theme-'.$_SESSION['theme'].'"';
        return "";
    }
?>