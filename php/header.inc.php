<?php
if (!isset($_SESSION['categories'])) {
	include 'php/varSession.inc.php';
}
?>

<header>
	<div id="header-img">
		<a href="index.php" title="Home page">
			<img src="img/logo.png">
		</a>
	</div>
	<div id="head2">
		<div id="societe">Arbres en folie</div>
		<ul>
			<li><a href="index.php">Accueil</a></li>
			<?php
			foreach ($_SESSION['categories'] as $categ) {
				echo '<li><a href="produits.php?category=' . $categ . '"> Arbres ' . ucfirst($categ) . '</a></li>';
			}
			?>
			<li><a href="contact.php">Contact</a></li>
		</ul>
		<div id="header-imgs">
			<?php if (!isset($_SESSION['user'])) : ?>
				<a href="connexion.php" id="btn-account" title="Se connecter">
				<?php else : ?>
					<a href="profile.php" id="btn-account" title="Mon compte">
					<?php endif ?>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
						<path d="M0 0h24v24H0z" fill="none" />
						<path d="M12 5.9c1.16 0 2.1.94 2.1 2.1s-.94 2.1-2.1 2.1S9.9 9.16 9.9 8s.94-2.1 2.1-2.1m0 9c2.97 0 6.1 1.46 6.1 2.1v1.1H5.9V17c0-.64 3.13-2.1 6.1-2.1M12 4C9.79 4 8 5.79 8 8s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 9c-2.67 0-8 1.34-8 4v3h16v-3c0-2.66-5.33-4-8-4z" />
					</svg>
					</a>
					<a href="panier.php" id="btn-panier" title="Mon panier" <?php if (!isset($_SESSION['user'])) echo "hidden"; ?>><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<path d="M0 0h24v24H0z" fill="none" />
							<path d="M7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zM1 2v2h2l3.6 7.59-1.35 2.45c-.16.28-.25.61-.25.96 0 1.1.9 2 2 2h12v-2H7.42c-.14 0-.25-.11-.25-.25l.03-.12.9-1.63h7.45c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.08-.14.12-.31.12-.48 0-.55-.45-1-1-1H5.21l-.94-2H1zm16 16c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z" />
						</svg>
					</a>
					<img src="img/empty.png" id="btn-theme" title="Changer de thème" alt="Use light theme">
		</div>
	</div>
</header>