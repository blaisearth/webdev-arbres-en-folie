<option value="">--Veuillez sélectionner votre métier--</option>
<option <?php if ($civilite == "Agriculteur") echo "selected"; ?>>Agriculteur</option>
<option <?php if ($civilite == "Artisan") echo "selected"; ?>>Artisan</option>
<option <?php if ($civilite == "Cadre/Chef d'entreprise") echo "selected"; ?>>Cadre/Chef d'entreprise</option>
<option <?php if ($civilite == "Salarié") echo "selected"; ?>>Salarié</option>
<option <?php if ($civilite == "Retraité") echo "selected"; ?>>Retraité</option>
<option <?php if ($civilite == "Etudiant") echo "selected"; ?>>Etudiant</option>
<option <?php if ($civilite == "Sans emploi") echo "selected"; ?>>Sans emploi</option>